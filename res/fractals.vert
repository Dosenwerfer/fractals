#version 450 core

in vec2 position;

out vec2 mandelbrotPos;

void main() {
    // Forward position
    mandelbrotPos = position;

    gl_Position = vec4(position, 0.0f, 1.0f);
}
