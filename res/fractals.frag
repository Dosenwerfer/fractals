#version 450 core

in vec2 mandelbrotPos;

out vec4 fragColor; // Output written to the screen is always location 0

// Fractal parameters
uniform int fractalType = 0; // 0: Mandelbrot, 1: Julia, 2: Mandelbulb, 3: MengerSponge
uniform float scale = 0;
uniform vec2 juliaOffset = vec2(0.36, 0.06);
uniform int MAX_ITER_2D = 200;
uniform int MAX_ITER_3D = 10;
uniform int MAX_RAYSTEPS = 150;
uniform int MANDELBULB_POW = 8;

uniform vec2 size;
uniform float aspectRatio = 1.0f;

// Camera
uniform vec3 cameraPosition = vec3(-0.5f, 0, -3.0f);
uniform float cameraRoll = 0.0f;
uniform float cameraPitch = 0.0f;
uniform float cameraYaw = 0.0f;
uniform float cameraFocalLength = 0.9f;

vec3 w = vec3(0,0,1);
vec3 v = vec3(0,1,0);
vec3 u = vec3(1,0,0);
mat3 cameraRotation;

/*
 * Calculates the mandelbrot value for a given complex number c.
 *
 * Returns 0 if c belongs to the set (not greater than 2 after MAX_ITER)
 * otherwise n as the number of iterations needed to determine
 * that c doesn't belong to the set.
 */
float mandelbrot(in vec2 z) {
    vec2 c = fractalType == 1 ? juliaOffset : z;
    int n = 0;
    while ((z.x * z.x + z.y * z.y) <= 4 && n < MAX_ITER_2D) {
        z = vec2((z.x * z.x - z.y * z.y) + c.x,
                 (z.x * z.y * 2.0) + c.y);
        ++n;
    }
    return n < MAX_ITER_2D ? float(n) / MAX_ITER_2D : 0;
}

/*
 * Distance estimator for Mandelbulb
 */
float mandelbulb(vec3 pos) {
    // TODO object rotation

    vec3 z = pos;
    float dr = 1.0;
    float r = sqrt(z.x*z.x + z.y*z.y + z.z*z.z);
    for (int i = 0; i < MAX_ITER_3D; ++i) {
        if (r > 5.0f) {
            break;
        }

        // Mandelbulb
        // Convert to polar coordinates
        float theta = asin(z.z / r);
        float phi = atan(z.y, z.x);

        // Update derivative
        dr = pow(r, MANDELBULB_POW - 1.0) * MANDELBULB_POW * dr + 1.0;

        // Scale and rotate the point
        float zr = pow(r, MANDELBULB_POW);
        theta = theta * MANDELBULB_POW;
        phi = phi * MANDELBULB_POW;

        // TODO optimize: calc cos(theta) just once
        // Convert back to cartesian coordinates
        //z = zr * vec3(cos(theta) * cos(phi), cos(theta) * sin(phi), sin(theta));
        z = zr * vec3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));

        z += pos;
        r = length(z);
    }

    return 0.5 * log(r) * r / dr;
}

/*
 * Distance estimator for MengerSponge
 */
float mengerSponge(vec3 pos) {
    // TODO object rotation
    pos = (pos * 0.5 + vec3(0.5)); // Scale [-1, 1] to [0, 1]

    vec3 v = abs(pos - vec3(0.5)) - vec3(0.5);
    float d1 = max(v.x, max(v.y, v.z));
    float d = d1;
    float p = 1.0;

    for (int i = 0; i < MAX_ITER_3D; ++i) {
        vec3 a = mod(3.0 * pos * p, 3.0);
        p = p * 3.0;

        v = vec3(0.5) - abs(a - vec3(1.5));

        // Distance inside the 3 axis aligned square tubes
        d1 = min(max(v.x, v.z), min(max(v.x, v.y), max(v.y, v.z))) / p;

        // Intersection
        d = max(d, d1);
    }

    return d * 2.0;
}

// March with distance estimate
float march(vec3 from, vec3 dir) {
    float rayLength = 0.0;
    int steps;
    vec3 ray;

    for (steps = 0; steps < MAX_RAYSTEPS; ++steps) {
        ray = from + rayLength * dir;
        float distance;
        if (fractalType == 2) {
            distance = mandelbulb(ray);
        } else if (fractalType == 3) {
            distance = mengerSponge(ray);
        }
        rayLength += distance;

        if (distance < 0.0001f) {
            break;
        }
    }

    return 1.0f - float(steps) / MAX_RAYSTEPS;
}

// Return rotation matrix for rotating around vector v by angle
mat3 rotationMatrixVector(vec3 v, float angle) {
    float c = cos(radians(angle));
    float s = sin(radians(angle));

    return mat3(c + (1.0 - c) * v.x * v.x, (1.0 - c) * v.x * v.y - s * v.z, (1.0 - c) * v.x * v.z + s * v.y,
              (1.0 - c) * v.x * v.y + s * v.z, c + (1.0 - c) * v.y * v.y, (1.0 - c) * v.y * v.z - s * v.x,
              (1.0 - c) * v.x * v.z - s * v.y, (1.0 - c) * v.y * v.z + s * v.x, c + (1.0 - c) * v.z * v.z);
}
vec3 rayDirection(vec2 pixel) {
    vec2 p = (0.5 * size - pixel) / vec2(size.x, -size.y);
    p.x *= aspectRatio;
    vec3 d = (p.x * u + p.y * v - cameraFocalLength * w);

    return normalize(cameraRotation * d);
}

// Color mapping
vec4 color(float iso) {
    vec3 rgb = vec3(0, 1, 243.0/255);
//    if (weight > 0.5) {
//        rgb = vec3(0, 1, 243.0/255); // Cyan
//    } else {
//        rgb = 2 * vec3(1, 0, 140.0/255); // Magenta
//    }
    return vec4(iso * rgb, 1);
}

void main() {
    // Calculate fractal
    float weight = 0;
    if (fractalType < 2) {
        // Mandelbrot and Julia
        weight = mandelbrot(vec2((mandelbrotPos.x + cameraPosition.x) / exp(scale),
                                 (mandelbrotPos.y + cameraPosition.y) / exp(scale)));
        // Intensify color
        weight = sqrt(weight);
    } else if (fractalType >= 2) {
        // Mandelbulb and MengerSponge
        // TODO outsource camera rotation calculation from shader to program
        cameraRotation = rotationMatrixVector(v, 180.0 - cameraYaw) * rotationMatrixVector(u, -cameraPitch) * rotationMatrixVector(w, cameraRoll);

        weight = march(cameraPosition, rayDirection(gl_FragCoord.xy));
    }

    // Determine color
    fragColor = color(weight);
}
