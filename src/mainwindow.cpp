#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGlobal>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Fractal mode controls
    connect(ui->rbMandelbrot, &QRadioButton::clicked,
                     ui->myOpenGLWidget, &MyGLWidget::setMandelbrot);
    connect(ui->rbJulia, &QRadioButton::clicked,
                     ui->myOpenGLWidget, &MyGLWidget::setJulia);
    connect(ui->rbMandelbulb, &QRadioButton::clicked,
                     ui->myOpenGLWidget, &MyGLWidget::setMandelbulb);
    connect(ui->rbMengerSponge, &QRadioButton::clicked,
                     ui->myOpenGLWidget, &MyGLWidget::setMengerSponge);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateMandelbrot,
                     ui->rbMandelbrot, &QRadioButton::setChecked);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateMandelbrot,
            [=]() {
        // Enable and disable according buttons
        ui->labelScale->setEnabled(true);
        ui->dsbScale->setEnabled(true);
        ui->dsbJuliaX->setEnabled(false);
        ui->labelJuliaX->setEnabled(false);
        ui->dsbJuliaY->setEnabled(false);
        ui->labelJuliaY->setEnabled(false);
        ui->cbAnimate->setEnabled(false);
        ui->labelMandelPower->setEnabled(false);
        ui->sbMandelPower->setEnabled(false);
        ui->labelZ->setEnabled(false);
        ui->dsbZ->setEnabled(false);
        ui->gbRotation->setEnabled(false);
        ui->labelFocalLength->setEnabled(false);
        ui->dsbFocalLength->setEnabled(false);
        reset();
    });
    connect(ui->myOpenGLWidget, &MyGLWidget::updateJulia,
                     ui->rbJulia, &QRadioButton::setChecked);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateJulia,
            [=]() {
        // Enable and disable according buttons
        ui->labelScale->setEnabled(true);
        ui->dsbScale->setEnabled(true);
        ui->labelJuliaX->setEnabled(true);
        ui->dsbJuliaX->setEnabled(true);
        ui->labelJuliaY->setEnabled(true);
        ui->dsbJuliaY->setEnabled(true);
        ui->cbAnimate->setEnabled(true);
        ui->labelMandelPower->setEnabled(false);
        ui->sbMandelPower->setEnabled(false);
        ui->labelZ->setEnabled(false);
        ui->dsbZ->setEnabled(false);
        ui->gbRotation->setEnabled(false);
        ui->labelFocalLength->setEnabled(false);
        ui->dsbFocalLength->setEnabled(false);
        reset();
    });
    connect(ui->myOpenGLWidget, &MyGLWidget::updateMandelbulb,
                     ui->rbMandelbulb, &QRadioButton::setChecked);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateMandelbulb,
            [=]() {
        // Enable and disable according buttons
        ui->labelScale->setEnabled(false);
        ui->dsbScale->setEnabled(false);
        ui->labelJuliaX->setEnabled(false);
        ui->dsbJuliaX->setEnabled(false);
        ui->labelJuliaY->setEnabled(false);
        ui->dsbJuliaY->setEnabled(false);
        ui->labelMandelPower->setEnabled(true);
        ui->sbMandelPower->setEnabled(true);
        ui->cbAnimate->setEnabled(true);
        ui->labelZ->setEnabled(true);
        ui->dsbZ->setEnabled(true);
        ui->gbRotation->setEnabled(true);
        ui->labelFocalLength->setEnabled(true);
        ui->dsbFocalLength->setEnabled(true);
        reset();
    });
    connect(ui->myOpenGLWidget, &MyGLWidget::updateMengerSponge,
                     ui->rbMengerSponge, &QRadioButton::setChecked);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateMengerSponge,
            [=]() {
        // Enable and disable according buttons
        ui->labelScale->setEnabled(false);
        ui->dsbScale->setEnabled(false);
        ui->labelJuliaX->setEnabled(false);
        ui->dsbJuliaX->setEnabled(false);
        ui->labelJuliaY->setEnabled(false);
        ui->dsbJuliaY->setEnabled(false);
        ui->cbAnimate->setEnabled(true);
        ui->labelMandelPower->setEnabled(false);
        ui->sbMandelPower->setEnabled(false);
        ui->labelZ->setEnabled(true);
        ui->dsbZ->setEnabled(true);
        ui->gbRotation->setEnabled(true);
        ui->labelFocalLength->setEnabled(true);
        ui->dsbFocalLength->setEnabled(true);
        reset();
    });

    // Fractal parameter controls
    // Scale
    connect(ui->dsbScale, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->myOpenGLWidget, &MyGLWidget::setScale);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateScale,
            ui->dsbScale, &QDoubleSpinBox::setValue);
    // Julia Offset X
    connect(ui->dsbJuliaX, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->myOpenGLWidget, &MyGLWidget::setJuliaOffsetX);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateJuliaOffsetX,
            ui->dsbJuliaX, &QDoubleSpinBox::setValue);
    // Julia Offset Y
    connect(ui->dsbJuliaY, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->myOpenGLWidget, &MyGLWidget::setJuliaOffsetY);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateJuliaOffsetY,
            ui->dsbJuliaY, &QDoubleSpinBox::setValue);
    // Animate
    connect(ui->cbAnimate, &QCheckBox::toggled,
            ui->myOpenGLWidget, &MyGLWidget::setAnimate);
    // Mandelbulb power
    connect(ui->sbMandelPower, QOverload<int>::of(&QSpinBox::valueChanged),
            ui->myOpenGLWidget, &MyGLWidget::setMandelPower);

    // Camera position controls
    connect(ui->dsbX, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->myOpenGLWidget, &MyGLWidget::setX);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateX,
            ui->dsbX, &QDoubleSpinBox::setValue);
    connect(ui->dsbY, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->myOpenGLWidget, &MyGLWidget::setY);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateY,
            ui->dsbY, &QDoubleSpinBox::setValue);
    connect(ui->dsbZ, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->myOpenGLWidget, &MyGLWidget::setZ);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateZ,
            ui->dsbZ, &QDoubleSpinBox::setValue);

    // Camera rotation controls
    connect(ui->sliderRoll, &QSlider::valueChanged,
            ui->myOpenGLWidget, &MyGLWidget::setRoll);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateRoll,
            ui->sliderRoll, &QSlider::setValue);
    connect(ui->sliderPitch, &QSlider::valueChanged,
            ui->myOpenGLWidget, &MyGLWidget::setPitch);
    connect(ui->myOpenGLWidget, &MyGLWidget::updatePitch,
            ui->sliderPitch, &QSlider::setValue);
    connect(ui->sliderYaw, &QSlider::valueChanged,
            ui->myOpenGLWidget, &MyGLWidget::setYaw);
    connect(ui->myOpenGLWidget, &MyGLWidget::updateYaw,
            ui->sliderYaw, &QSlider::setValue);

    connect(ui->dsbFocalLength, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->myOpenGLWidget, &MyGLWidget::setFocalLength);

    // Reset
    connect(ui->buttonReset, &QPushButton::pressed,
            this, &MainWindow::reset);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::reset() {
    switch (ui->myOpenGLWidget->getFractal()) {
    case MANDELBROT:
        // Fractal parameters
        ui->myOpenGLWidget->setScale(0);

        // Camera position
        ui->myOpenGLWidget->setX(-0.5f);
        ui->myOpenGLWidget->setY(0);

        break;
    case JULIA:
        // Fractal parameters
        ui->myOpenGLWidget->setScale(-0.5f);
        // TODO

        // Camera position
        ui->myOpenGLWidget->setX(0);
        ui->myOpenGLWidget->setY(0);

        break;
    case MANDELBULB:
        // Fractal parameters
        ui->myOpenGLWidget->setMandelPower(8);

        // Camera position
        ui->myOpenGLWidget->setX(0);
        ui->myOpenGLWidget->setY(0);
        ui->myOpenGLWidget->setZ(-2.0f);

        // Camera rotation
        ui->sliderRoll->setValue(0);
        ui->sliderPitch->setValue(0);
        ui->sliderYaw->setValue(0);

        ui->dsbFocalLength->setValue(0.9);
        break;
    case MENGERSPONGE:
        // Camera position
        ui->myOpenGLWidget->setX(0);
        ui->myOpenGLWidget->setY(0);
        ui->myOpenGLWidget->setZ(-3.0f);

        // Camera rotation
        ui->sliderRoll->setValue(0);
        ui->sliderPitch->setValue(0);
        ui->sliderYaw->setValue(0);

        ui->dsbFocalLength->setValue(0.9);
        break;
    default:
        break;
    }
}
