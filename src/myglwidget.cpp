#include "myglwidget.h"

#include <iostream>
#include <math.h>
#include <stack>
#include <QWidget>
#include <QMainWindow>
#include <QKeyEvent>

MyGLWidget::MyGLWidget(QWidget *parent) :
    QOpenGLWidget(parent),
    timer(new QElapsedTimer()),
    m_cameraPos(QVector3D(-0.5f, 0.0f, -3.0f))
{
    // Focus widget so it listens on keyboard events
    setFocusPolicy(Qt::StrongFocus);

    // Set OpenHandCursor
    this->setCursor(Qt::OpenHandCursor);

    // Start timer
    timer->start();
}

// DESTRUCT
MyGLWidget::~MyGLWidget() {
    makeCurrent();

    delete m_prog;
    delete timer;
    delete debugLogger;
}

// INITIALIZE
void MyGLWidget::initializeGL() {
    // Initialize debug logging
    debugLogger = new QOpenGLDebugLogger(this);
    connect(debugLogger, &QOpenGLDebugLogger::messageLogged,
            this, &MyGLWidget::onMessageLogged);
    if (debugLogger->initialize()) {
        debugLogger->startLogging(QOpenGLDebugLogger::SynchronousLogging);
        debugLogger->enableMessages();
    }

    // Initialize gl functions
    bool success = initializeOpenGLFunctions();
    Q_ASSERT(success);
    Q_UNUSED(success);

    //// Set gl parameters
    // Set clear color
    glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
    // Enable alpha blending
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // Enable face culling
    glEnable(GL_CULL_FACE);
    // Use MSAA
    //glEnable(GL_MULTISAMPLE);

    //// Load shaders
    m_prog = new QOpenGLShaderProgram();
    m_prog->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/res/fractals.vert");
    m_prog->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/res/fractals.frag");
    m_prog->link();
    Q_ASSERT(m_prog->isLinked());

    // Initialize vbo
    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    // Fill vbo
    Vertex vert[] = {{-1,1}, {-1,-1}, {1,-1}, {1,1}};
    glBufferData(GL_ARRAY_BUFFER, sizeof(vert), vert, GL_STATIC_DRAW);

    // Initialize vao
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);
    // Fill vao
    // define helper for offsetof that does the void* cast
    #define OFS(s, a) reinterpret_cast<void* const>(offsetof(s, a))
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), OFS(Vertex, position));
    #undef OFS

    // Initialize ibo
    glGenBuffers(1, &m_ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    // Fill ibo
    GLuint data[] = { 0, 1, 2, 0, 2, 3 };
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*4, data, GL_STATIC_DRAW);

    glBindVertexArray(0);
}

// RENDERING
void MyGLWidget::paintGL() {
    // Limit FPS on 3D Fractals
//    const int FPS_3D = 5;
//    if (m_fractal >= 2 && timer->elapsed() % (1000/FPS_3D) != 0) {
//        return;
//    }

    // Clear canvas
    glClear(GL_COLOR_BUFFER_BIT);

    // Bind shader
    m_prog->bind();

    //// Set uniforms
    // Camera position
    m_prog->setUniformValue("cameraPosition", m_cameraPos);

    // Camera rotation
    m_prog->setUniformValue("cameraRoll", float(m_camRoll));
    m_prog->setUniformValue("cameraPitch", float(m_camPitch));
    m_prog->setUniformValue("cameraYaw", float(m_camYaw));

    // Fractals
    m_prog->setUniformValue("fractalType", m_fractal);
    m_prog->setUniformValue("scale", m_scale);
    if (m_fractal == JULIA && m_animate) {
        setJuliaOffsetX(cos(timer->elapsed() / 800.0));
        setJuliaOffsetY(sin(timer->elapsed() / 1000.0));
    }
    m_prog->setUniformValue("juliaOffset", QVector2D(m_juliaOffsetX, m_juliaOffsetY));
    m_prog->setUniformValue("MANDELBULB_POW", m_mandelPower);
    m_prog->setUniformValue("size", QVector2D(width(), height()));

    //// Draw objects
    glBindVertexArray(m_vao);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

    update();
}
void MyGLWidget::resizeGL(int w, int h) {

}

void MyGLWidget::mousePressEvent(QMouseEvent *event) {
    if (event->button() != Qt::LeftButton) {
        return;
    }
    this->setCursor(Qt::ClosedHandCursor);
    m_isDragging = true;
    m_preDragPos = event->pos();
}
void MyGLWidget::mouseMoveEvent(QMouseEvent *event) {
    QVector2D move = QVector2D(event->pos() - m_preDragPos);
    setX(m_cameraPos.x() - move.x() * 2 / width());
    setY(m_cameraPos.y() + move.y() * 2 / height());
    m_preDragPos = event->pos();
}
void MyGLWidget::mouseReleaseEvent(QMouseEvent *event) {
    if (event->button() != Qt::LeftButton) {
        return;
    }
    this->setCursor(Qt::OpenHandCursor);
    m_isDragging = false;
}
void MyGLWidget::wheelEvent(QWheelEvent *event) {
    setScale(m_scale + float(event->angleDelta().y()) / 256);
}
void MyGLWidget::keyPressEvent(QKeyEvent *event) {
    const float DELTA_MOVE = 0.02f;
    const int DELTA_ROT = 2;
    switch (event->key()) {
    case Qt::Key_W: {
        if (m_fractal >= 2) {
            // Move forward
            m_cameraPos.setZ(m_cameraPos.z() + DELTA_MOVE);
        } else {
            // Zoom in
            setScale(m_scale + 0.1f);
        }
        break;
    }
    case Qt::Key_A: {
        // Move left
        m_cameraPos.setX(m_cameraPos.x() - DELTA_MOVE);
        break;
    }
    case Qt::Key_S: {
        if (m_fractal >= 2) {
            // Move back
            m_cameraPos.setZ(m_cameraPos.z() - DELTA_MOVE);
        } else {
            // Zoom out
            setScale(m_scale - 0.1f); // TODO good?
        }
        break;
    }
    case Qt::Key_D: {
        // Move right
        m_cameraPos.setX(m_cameraPos.x() + DELTA_MOVE);
        break;
    }
    case Qt::Key_E: {
        // Ascend
        setY(m_cameraPos.y() + DELTA_MOVE);
        break;
    }
    case Qt::Key_Q: {
        // Descend
        setY(m_cameraPos.y() - DELTA_MOVE);
        break;
    }
    case Qt::Key_Left: {
        if (m_fractal >= 2) {
            // Turn left
            setYaw(m_camYaw - DELTA_ROT);
        }
        break;
    }
    case Qt::Key_Right: {
        if (m_fractal >= 2) {
            // Turn right
            setYaw(m_camYaw + DELTA_ROT);
        }
        break;
    }
    case Qt::Key_Up: {
        if (m_fractal >= 2) {
            // Turn up
            setPitch(m_camPitch + DELTA_ROT);
        }
        break;
    }
    case Qt::Key_Down: {
        if (m_fractal >= 2) {
            // Turn down
            setPitch(m_camPitch - DELTA_ROT);
        }
        break;
    }
    default: {
        // Call base method
        QOpenGLWidget::keyPressEvent(event);
        break;
    }
    }
}

// Slots
// Fractal type
void MyGLWidget::setMandelbrot(bool value) {
    if (value) {
        if (m_fractal != MANDELBROT) {
            m_fractal = MANDELBROT;
            emit updateMandelbrot(true);
        }
    }
}
void MyGLWidget::setJulia(bool value) {
    if (value) {
        if (m_fractal != JULIA) {
            m_fractal = JULIA;
            emit updateJulia(true);
        }
    }
}
void MyGLWidget::setMandelbulb(bool value) {
    if (value) {
        if (m_fractal != MANDELBULB) {
            m_fractal = MANDELBULB;
            emit updateMandelbulb(true);
        }
    }
}
void MyGLWidget::setMengerSponge(bool value) {
    if (value) {
        if (m_fractal != MENGERSPONGE) {
            m_fractal = MENGERSPONGE;
            emit updateMengerSponge(true);
        }
    }
}

// Fractal parameters
void MyGLWidget::setScale(double value) {
    if (m_scale != value) {
        // Adapt offset
        setX(m_cameraPos.x() / exp(m_scale) * exp(value));
        setY(m_cameraPos.y() / exp(m_scale) * exp(value));

        // Adapt scale
        this->m_scale = value;
        emit updateScale(value);
    }
}
void MyGLWidget::setJuliaOffsetX(double value) {
    if (m_juliaOffsetX != (float) value) {
        m_juliaOffsetX = value;
        emit updateJuliaOffsetX(value);
    }
}
void MyGLWidget::setJuliaOffsetY(double value) {
    if (m_juliaOffsetY != (float) value) {
        m_juliaOffsetY = value;
        emit updateJuliaOffsetY(value);
    }
}
void MyGLWidget::setMandelPower(int value) {
    if (m_mandelPower != value) {
        m_mandelPower = value;
    }
}
void MyGLWidget::setAnimate(bool value) {
    if (m_animate != value) {
        m_animate = value;
    }
}

// Camera position
void MyGLWidget::setX(double value) {
    if (m_cameraPos.x() != value) {
        this->m_cameraPos.setX(value);
        emit updateX(value);
    }
}
void MyGLWidget::setY(double value) {
    if (m_cameraPos.y() != value) {
        this->m_cameraPos.setY(value);
        emit updateY(value);
    }
}
void MyGLWidget::setZ(double value) {
    if (m_cameraPos.z() != value) {
        this->m_cameraPos.setZ(value);
        emit updateZ(value);
    }
}
void MyGLWidget::printCameraPos() {
    std::cout << "Camera position: x=" << m_cameraPos.x()
              << ", y=" << m_cameraPos.y()
              << ", z=" << m_cameraPos.z() << std::endl;
}

// Camera rotation
void MyGLWidget::setRoll(int value) {
    if (m_camRoll != value) {
        this->m_camRoll = value;
        emit updateRoll(value);
    }
}
void MyGLWidget::setPitch(int value) {
    if (m_camPitch != value) {
        this->m_camPitch = value;
        emit updatePitch(value);
    }
}
void MyGLWidget::setYaw(int value) {
    if (m_camYaw != value) {
        this->m_camYaw = value;
        emit updateYaw(value);
    }
}
void MyGLWidget::setFocalLength(double value) {
    if (m_camFocalLength != value) {
        this->m_camFocalLength = value;
        emit updateFocalLength(value);
    }
}

void MyGLWidget::onMessageLogged(QOpenGLDebugMessage message) {
    // Suppress FBO warning
    if (message.message().contains("API_ID_REDUNDANT_FBO")) {
        return;
    }
    std::cout << message.message().toStdString() << std::endl;
}
