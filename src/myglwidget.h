#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QOpenGLContext>
#include <QOpenGLWidget>
#include <QVector3D>
#include <QOpenGLDebugLogger>
#include <QOpenGLFunctions_4_5_Core>
#include <QOpenGLShaderProgram>
#include <QElapsedTimer>
#include <string>

struct Vertex {
    GLfloat position[2];
};

enum FractalType {
    MANDELBROT, JULIA, MANDELBULB, MENGERSPONGE
};

class MyGLWidget : public QOpenGLWidget, protected QOpenGLFunctions_4_5_Core
{
    Q_OBJECT

public:
    MyGLWidget(QWidget *parent);
    ~MyGLWidget();

    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

    FractalType getFractal() { return m_fractal; }

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);

public slots:
    void onMessageLogged(QOpenGLDebugMessage message);

    // Fractals
    void setMandelbrot(bool value);
    void setJulia(bool value);
    void setMandelbulb(bool value);
    void setMengerSponge(bool value);

    void setScale(double value);
    void setJuliaOffsetX(double value);
    void setJuliaOffsetY(double value);
    void setMandelPower(int value);
    void setAnimate(bool value);

    // Camera position
    void setX(double value);
    void setY(double value);
    void setZ(double value);

    // Camera rotation
    void setRoll(int value);
    void setPitch(int value);
    void setYaw(int value);
    void setFocalLength(double value);

signals:
    void updateMandelbrot(bool);
    void updateJulia(bool);
    void updateMandelbulb(bool);
    void updateMengerSponge(bool);

    void updateScale(double);
    void updateJuliaOffsetX(double);
    void updateJuliaOffsetY(double);

    void updateX(double);
    void updateY(double);
    void updateZ(double);

    void updateRoll(int);
    void updatePitch(int);
    void updateYaw(int);
    void updateFocalLength(double);

private:
    // Misc
    QOpenGLDebugLogger* debugLogger;
    QElapsedTimer* timer;

    // GL buffers and shaders
    GLuint m_vbo;
    GLuint m_vao;
    GLuint m_ibo;
    QOpenGLShaderProgram* m_prog;

    // Fractal
    FractalType m_fractal = MANDELBROT;
    float m_scale = 0;
    float m_juliaOffsetX = 0.36;
    float m_juliaOffsetY = 0.06;
    int m_mandelPower = 8;
    bool m_animate = true;

    // Camera position
    QVector3D m_cameraPos;
    QPoint m_preDragPos;
    bool m_isDragging = false;
    void printCameraPos();

    // Camera rotation
    int m_camRoll = 0;
    int m_camPitch = 0;
    int m_camYaw = 0;
    float m_camFocalLength = 0.9f;
};

#endif // MYGLWIDGET_H
