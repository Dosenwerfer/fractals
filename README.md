# Fractals

Explore the 2D fractals Mandelbrot and Julia Set and the 3D fractals Mandelbulb and Menger Sponge with this application I made using OpenGL.

Check it out in this quick [video demonstration](https://www.youtube.com/watch?v=_k4DF1MgC5s).

## Screenshots

<p float="left">
  <img src="https://gitlab.com/Dosenwerfer/fractals/raw/master/doc/Screenshot_Mandelbrot.jpg" width="49%" alt="Mandelbrot Set" title="Mandelbrot Set" />
  <img src="https://gitlab.com/Dosenwerfer/fractals/raw/master/doc/Screenshot_Julia.jpg" width="49%" alt="Julia Set" title="Julia Set" />
  <br />
  <img src="https://gitlab.com/Dosenwerfer/fractals/raw/master/doc/Screenshot_Mandelbulb.jpg" width="49%" alt="Mandelbulb Set" title="Mandelbulb Set" />
  <img src="https://gitlab.com/Dosenwerfer/fractals/raw/master/doc/Screenshot_MengerSponge.jpg" width="49%" alt="Menger Sponge Set" title="Menger Sponge Set" />
</p>

## Prerequisites

* [Qt Framework](https://www.qt.io/) - The GUI framework used
* [MinGW](https://mingw-w64.org/) - Compiler
* OpenGL enabled GPU

## Authors

* **Dosenwerfer** - [Dosenwerfer](https://gitlab.com/Dosenwerfer)
